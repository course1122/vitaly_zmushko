package com.example.homework.ext

import com.example.domain.dto.LanguageDTO

fun MutableList<LanguageDTO>.rebuildListLanguages(): String {
    var nameString = ""
    this.forEachIndexed { _, language ->
        nameString += "$language "
    }
    return nameString
}