package com.example.homework.di

import androidx.lifecycle.SavedStateHandle
import com.example.homework.fragments.countryList.BlankFragment1
import com.example.homework.fragments.countryList.CountryListViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val  countryListModule = module {

    scope<BlankFragment1>{

        viewModel { (handle: SavedStateHandle) -> CountryListViewModel(handle, get(), get()) }

    }

}