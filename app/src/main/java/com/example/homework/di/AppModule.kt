package com.example.homework.di

import com.example.data.ext.ListCapitalsToListCapitalsDto
import com.example.data.model.ModelItem
import com.example.data.network.CoroutinesRetrofitCreator
import com.example.data.network.FlowRetrofitCreator
import com.example.data.network.NewsRetrofitCreator
import com.example.data.network.RetrofitCreator
import com.example.data.newsModel.Article
import com.example.data.newsModel.NewsApiModel
import com.example.domain.dto.ArticleDTO
import com.example.domain.dto.ModelItemDTO
import com.example.domain.outcome.Transformer
import com.example.homework.database.DataBase
import com.example.homework.repository.database.DatabaseRepository
import com.example.homework.repository.database.DatabaseRepositoryImpl
import com.example.domain.repository.NetworkRepository
import com.example.homework.repository.networkRepo.NetworkRepositoryImpl
import com.example.domain.repository.NetworkRepositoryWithCoroutines
import com.example.domain.repository.NetworkCapitalsRepositoryFlow
import com.example.domain.repository.NetworkRepositoryNews
import com.example.homework.dtotransform.NewsApiModelToNewsApiModelDTO
import com.example.homework.repository.flow.NetworkRepositoryWithFlowImpl
import com.example.homework.repository.networkRepoWithCoroutines.NetworkRepositoryWithCoroutinesImpl
import com.example.homework.repository.news.NetworkRepositoryNewsImpl
import org.koin.core.qualifier.named
import org.koin.core.scope.get
import org.koin.dsl.module

val appModule = module {

    single { DataBase.init(get()) }

    single { RetrofitCreator().getRetrofit() }

    single { CoroutinesRetrofitCreator().getCoroutinesRetrofit() }

    single { FlowRetrofitCreator().getFlowCoroutinesRetrofit() }

    single { NewsRetrofitCreator().getNewsRetrofit() }

    single <DatabaseRepository>{ DatabaseRepositoryImpl(get()) }

    single <NetworkRepository>{ NetworkRepositoryImpl(get()) }

    single <NetworkRepositoryWithCoroutines>{ NetworkRepositoryWithCoroutinesImpl(get()) }

    single <NetworkCapitalsRepositoryFlow>{ NetworkRepositoryWithFlowImpl(get(), get(named("CapitalsTransformer"))) }

    single <NetworkRepositoryNews>{ NetworkRepositoryNewsImpl(get(), get(named("NewsTransformer"))) }

    single <Transformer<List<ModelItem>, List<ModelItemDTO>>>(named("CapitalsTransformer")) { ListCapitalsToListCapitalsDto() }

    single <Transformer<NewsApiModel, List<ArticleDTO>>>(named("NewsTransformer")) { NewsApiModelToNewsApiModelDTO() }

}