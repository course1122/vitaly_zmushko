package com.example.homework.di

import androidx.lifecycle.SavedStateHandle
import com.example.homework.fragments.news.NewsFragment
import com.example.homework.fragments.news.NewsFragmentViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val newsModule = module {

    scope<NewsFragment> {

        viewModel { (handle: SavedStateHandle) ->
            NewsFragmentViewModel(handle,
                get())
        }

    }
}