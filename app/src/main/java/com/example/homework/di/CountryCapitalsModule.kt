package com.example.homework.di

import androidx.lifecycle.SavedStateHandle
import com.example.homework.fragments.fragmentCountryInfoWithCoroutines.CapitalCountriesFragmentViewModel
import com.example.homework.fragments.fragmentCountryInfoWithCoroutines.FragmentCountryInfoWithCoroutines
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val countryListCapitalsModule = module {

    scope<FragmentCountryInfoWithCoroutines> {

        viewModel { (handle: SavedStateHandle) -> CapitalCountriesFragmentViewModel(handle, get(), get()) }

    }
}