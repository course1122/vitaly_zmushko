package com.example.homework.repository.news

import com.example.data.ext.modifyFlow
import com.example.data.network.NewsRetrofitService
import com.example.data.newsModel.Article
import com.example.data.newsModel.NewsApiModel
import com.example.domain.dto.ArticleDTO
import com.example.domain.outcome.Outcome
import com.example.domain.outcome.Transformer
import com.example.domain.repository.NetworkRepositoryNews
import kotlinx.coroutines.flow.Flow

class NetworkRepositoryNewsImpl(
    private val mService: NewsRetrofitService,
    private val capitalListTransformer: Transformer<NewsApiModel, List<ArticleDTO>>,
) : NetworkRepositoryNews {

    override fun getNews(codeCountry: String): Flow<Outcome<List<ArticleDTO>>> =
        modifyFlow(mService.getNewsByCountry(codeCountry), capitalListTransformer)

}