package com.example.homework.repository.database

import com.example.homework.database.TableCountry

interface DatabaseRepository {

    fun insertData(entity: TableCountry)

    fun addAll(list: List<TableCountry>)

    fun getInfoCountries(): List<TableCountry>

    fun getInfoCountriesName(name: String): List<TableCountry>

}