package com.example.homework.repository.networkRepoWithCoroutines

import com.example.domain.dto.ModelItemDTO
import com.example.data.network.RetrofitCoroutinesService

class NetworkRepositoryWithCoroutinesImpl(private val mService: RetrofitCoroutinesService) :
    com.example.domain.repository.NetworkRepositoryWithCoroutines {

    override suspend fun getCapitalsListAsync(): MutableList<ModelItemDTO> = mService.getCapitalListAsync()

}