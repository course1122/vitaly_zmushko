package com.example.homework.repository.database

import com.example.homework.database.DataBase
import com.example.homework.database.TableCountry

class DatabaseRepositoryImpl(private val base: DataBase) :
    DatabaseRepository {

    override fun insertData(entity: TableCountry) {
        base.getInfoCountryDAO().insertData(entity = entity)
    }

    override fun addAll(list: List<TableCountry>) {
        base.getInfoCountryDAO().addAll(list = list)
    }

    override fun getInfoCountries(): List<TableCountry> {
        return base.getInfoCountryDAO().getInfoCountries()
    }

    override fun getInfoCountriesName(name: String): List<TableCountry> {
        return base.getInfoCountryDAO().getInfoCountriesName(name)
    }
}