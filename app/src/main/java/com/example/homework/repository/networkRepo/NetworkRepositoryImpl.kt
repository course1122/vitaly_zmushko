package com.example.homework.repository.networkRepo

import com.example.domain.dto.ModelItemDTO
import com.example.data.network.RetrofitService
import io.reactivex.rxjava3.core.Flowable

class NetworkRepositoryImpl(private val mService: com.example.data.network.RetrofitService) :
    com.example.domain.repository.NetworkRepository {

    override fun getNameList(): Flowable<MutableList<ModelItemDTO>> = mService.getNameList()

    override fun getCountryByName(name: String): Flowable<MutableList<ModelItemDTO>> = mService.getCountryByName(name)

}