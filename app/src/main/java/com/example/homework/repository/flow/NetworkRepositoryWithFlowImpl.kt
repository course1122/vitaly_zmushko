package com.example.homework.repository.flow

import com.example.data.ext.modifyFlow
import com.example.data.network.FlowRetrofitService
import com.example.domain.dto.ModelItemDTO
import com.example.domain.outcome.Outcome
import com.example.domain.outcome.Transformer
import com.example.domain.repository.NetworkCapitalsRepositoryFlow
import com.example.data.model.ModelItem
import kotlinx.coroutines.flow.Flow

class NetworkRepositoryWithFlowImpl(
    private val mService: FlowRetrofitService,
    private val capitalListTransformer: Transformer<List<ModelItem>, List<ModelItemDTO>>
    ) : NetworkCapitalsRepositoryFlow {

    override fun getCapitalsList(): Flow<Outcome<List<ModelItemDTO>>> =
        modifyFlow(mService.getNameList(), capitalListTransformer)

    override fun getCapitalByName(capitalName: String): Flow<Outcome<List<ModelItemDTO>>> =
        modifyFlow(mService.getCountryByName(capitalName), capitalListTransformer)

}