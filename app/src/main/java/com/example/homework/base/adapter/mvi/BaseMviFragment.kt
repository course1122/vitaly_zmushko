package com.example.homework.base.adapter.mvi

import android.os.Bundle
import org.koin.androidx.scope.ScopeFragment

abstract class BaseMviFragment<
        INTENT : ViewIntent, ACTION : ViewAction, STATE : ViewState,
        ViewModel : BaseViewModel<INTENT, ACTION, STATE>,
        > :
    IViewRenderer<STATE>, ScopeFragment() {
    lateinit var viewState: STATE
    val mState get() = viewState

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initData()
    }

    abstract fun initUI()
    abstract fun initData()
    abstract fun dispatchIntent(intent: INTENT)
}