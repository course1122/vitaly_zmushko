package com.example.homework.base.adapter.mvi

import androidx.lifecycle.LiveData

interface IModel<STATE : ViewState, INTENT : ViewIntent> {

    val state: LiveData<STATE>

    fun dispatchIntent(intent: INTENT)

}
