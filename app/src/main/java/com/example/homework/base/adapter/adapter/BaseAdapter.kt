package com.example.homework.base.adapter.adapter

import androidx.recyclerview.widget.RecyclerView

abstract class BaseAdapter<ItemType> : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    protected var mDataList: MutableList<ItemType> = mutableListOf()

    protected var mOnItemClickListener: ((ItemType) -> Unit?)? = null

    fun setItemClick(clickListener: (ItemType) -> Unit) {
        mOnItemClickListener = clickListener
    }

    override fun getItemCount(): Int = mDataList.size

    interface OnItemClickListener<ItemType> {
        fun onClick(item: ItemType)
    }

    open fun repopulate(list: MutableList<ItemType>) {
        mDataList.clear()
        mDataList.addAll(list)
        notifyDataSetChanged()
    }

    open fun addList(list: MutableList<ItemType>) {
        mDataList.addAll(list)
        notifyDataSetChanged()
    }

    open fun addItem(item: ItemType) {
        mDataList.add(item)
        notifyItemChanged(mDataList.size - 1)
    }

}