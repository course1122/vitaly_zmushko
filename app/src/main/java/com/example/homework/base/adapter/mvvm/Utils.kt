package com.example.homework.base.adapter.mvvm

import androidx.lifecycle.MutableLiveData
import com.example.domain.outcome.Outcome
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.disposables.Disposable


fun <T> executeJob(job: Flowable<T>, outcome: MutableLiveData<Outcome<T>>): Disposable {
    outcome.loading(true)
    return job.executeOnIoThread()
        .subscribe({
            outcome.next(it)
        }, {
            outcome.failed(it)
        }, {
            if (outcome.value is Outcome.Next) {
                outcome.success((outcome.value as Outcome.Next).data)
            }
        })
}

fun <T> executeJobOnInitialThread(
    job: Flowable<T>,
    outcome: MutableLiveData<Outcome<T>>
): Disposable {
    outcome.loading(true)
    return job.subscribe({
        outcome.next(it)
    }, {
        outcome.failed(it)
    }, {
        if (outcome.value is Outcome.Next) {
            outcome.success((outcome.value as Outcome.Next).data)
        }
    })
}

fun <T> executeJob(job: Single<T>, outcome: MutableLiveData<Outcome<T>>): Disposable {
    outcome.loading(true)
    return job.executeOnIoThread()
        .subscribe({
            outcome.success(it)
        }, {
            outcome.failed(it)
        })
}

fun executeJob(job: Completable, outcome: MutableLiveData<Outcome<Any>>): Disposable {
    outcome.loading(true)
    return job.executeOnIoThread()
        .subscribe({
            outcome.success(Any())
        }, {
            outcome.failed(it)
        })
}

fun <T> executeJobWithoutProgress(
    job: Flowable<T>,
    outcome: MutableLiveData<Outcome<T>>
): Disposable {
    return job.executeOnIoThread()
        .subscribe({
            outcome.next(it)
        }, {
            outcome.failed(it)
        }, {
            if (outcome.value is Outcome.Next) {
                outcome.success((outcome.value as Outcome.Next).data)
            }
        })
}

fun <T> MutableLiveData<Outcome<T>>.loading(isLoading: Boolean) {
    this.value = Outcome.loading(isLoading)
}

fun <T> MutableLiveData<Outcome<T>>.success(t: T) {
    with(this) {
        loading(false)
        value = Outcome.success(t)
    }
}

fun <T> MutableLiveData<Outcome<T>>.next(t: T) {
    with(this) {
        loading(false)
        value = Outcome.next(t)
    }
}

fun <T> MutableLiveData<Outcome<T>>.failed(e: Throwable) {
    with(this) {
        loading(false)
        value = Outcome.failure(e)
    }
}
