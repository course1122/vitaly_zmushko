package com.example.homework.base.adapter.mvp

interface BaseMvpView {

    fun showError(error: String, throwable: Throwable)

    fun showProgress()

    fun hideProgress()

}
