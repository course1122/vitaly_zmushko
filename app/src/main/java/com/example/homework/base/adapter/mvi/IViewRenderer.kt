package com.example.homework.base.adapter.mvi

interface IViewRenderer<STATE> {

    fun render(state: STATE)

}