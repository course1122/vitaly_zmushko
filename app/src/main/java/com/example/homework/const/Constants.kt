package com.example.homework.const

const val BASE_URL = "https://restcountries.eu/rest/v2/"
const val FILE_NAME_SAVED_STANCE = "SavedStance"
const val SAVED_STANCE = "Saved"
const val ERROR = "ERROR"

const val MIN_PEOPLE = 0F
const val MAX_PEOPLE = 1377422168F

const val GET_ALL_INFO_COUNTRIES = "all"
const val GET_ALL_CAPITAL = "all?fields=capital"

const val PATH_VARIABLE = "path"
const val NAME_PATH_URL = "/{$PATH_VARIABLE}"

const val GET_COUNTRY_BY_NAME = "name$NAME_PATH_URL"

const val MIN_SYMBOLS_ON_SEARCH = 2
const val MILLISECOND: Long = 500