package com.example.homework.fragments.news

import com.example.homework.adapters.NewsLocationRecyclerAdapter
import com.example.homework.base.adapter.mvi.BaseMviFragment
import org.koin.androidx.viewmodel.ext.android.stateViewModel

class NewsFragmentMvi : BaseMviFragment<NewsIntent, NewsAction, NewsState, NewsViewModelMvi>() {

    lateinit var adapter: NewsLocationRecyclerAdapter
    private val viewModel: NewsViewModelMvi by stateViewModel()
    override fun initUI() {
        TODO("Not yet implemented")
    }

    override fun initData() {
        viewModel.dispatchIntent(NewsIntent.LoadAllNews)
    }

    override fun dispatchIntent(intent: NewsIntent) {
        TODO("Not yet implemented")
    }

    override fun render(state: NewsState) {

    }
}