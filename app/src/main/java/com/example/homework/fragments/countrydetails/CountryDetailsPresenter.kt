package com.example.homework.fragments.countrydetails

import com.example.homework.base.adapter.mvp.BaseMvpPresenter
import com.example.data.network.RetrofitCreator
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers

class CountryDetailsPresenter : BaseMvpPresenter<CountryDetailsView>() {

    private var retroBuild = RetrofitCreator().getRetrofit()
    lateinit var mCountryName: String

    fun getItemByCountryName(countryName: String, isRefresh: Boolean) {
        retroBuild.getCountryByName(countryName)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
        addDisposable(
            inBackground(
                handleProgress(
                    retroBuild.getCountryByName(countryName), isRefresh
                )
            )
                .subscribe({ response ->
                    getView()?.showCountryInfo(response)
                },
                    {
                        it.message?.let { it1 -> getView()?.showError(it1, it) }
                    })
        )
    }


}