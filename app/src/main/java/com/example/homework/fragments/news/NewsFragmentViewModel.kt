package com.example.homework.fragments.news


import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import com.example.domain.dto.ArticleDTO
import com.example.domain.outcome.Outcome
import com.example.domain.repository.NetworkRepositoryNews
import com.example.homework.base.adapter.mvvm.BaseViewModel
import kotlinx.coroutines.flow.Flow

class NewsFragmentViewModel(
    savedStateHandle: SavedStateHandle,
    private val mNetworkNewsRepositoryFlow: NetworkRepositoryNews,
) : BaseViewModel(savedStateHandle) {

    private val mCountryCapitalsLiveData =
        savedStateHandle.getLiveData<Outcome<List<ArticleDTO>>>("NewsDTO")

    fun getLiveData(): MutableLiveData<Outcome<List<ArticleDTO>>> {
        return mCountryCapitalsLiveData
    }

    fun getNews(): Flow<Outcome<List<ArticleDTO>>> =
        mNetworkNewsRepositoryFlow.getNews("ru")

}
