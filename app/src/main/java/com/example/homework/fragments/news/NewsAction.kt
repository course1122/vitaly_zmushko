package com.example.homework.fragments.news

import com.example.homework.base.adapter.mvi.ViewAction

sealed class NewsAction : ViewAction {
    object AllNews : NewsAction()
}