package com.example.homework.fragments.news

import com.example.homework.base.adapter.mvi.ViewIntent

sealed class NewsIntent : ViewIntent {
    object LoadAllNews : NewsIntent()
}
