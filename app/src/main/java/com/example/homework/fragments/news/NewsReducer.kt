package com.example.homework.fragments.news

import com.example.data.newsModel.NewsApiModel
import com.example.domain.dto.ArticleDTO
import com.example.domain.outcome.Outcome

fun Outcome<List<ArticleDTO>>.reduce(): NewsState {
    return when (this) {
        is Outcome.Success -> NewsState.ResultAllNews(data)
        is Outcome.Progress -> NewsState.Loading(loading)
        is Outcome.Failure -> NewsState.Exception(e)
        else -> NewsState.Exception(Throwable(""))
    }
}
