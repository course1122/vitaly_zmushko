package com.example.homework.fragments.countryList

import android.content.Context
import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.setFragmentResultListener
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.homework.R
import com.example.homework.adapters.RecyclerAdapter
import com.example.domain.outcome.Outcome
import com.example.homework.bundle.COUNTRY_NAME_KEY
import com.example.homework.const.FILE_NAME_SAVED_STANCE
import com.example.homework.const.SAVED_STANCE
import com.example.homework.database.DataBase
import com.example.homework.database.convertDBData
import com.example.domain.dto.ModelItemDTO
import org.koin.androidx.scope.ScopeFragment
import org.koin.androidx.viewmodel.ext.android.stateViewModel

// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

class BlankFragment1 : ScopeFragment() {
    var param1: String? = null
    private var param2: String? = null

    private var flag: Boolean = true
    private lateinit var recyclerView: RecyclerView

    private var mAdapter = RecyclerAdapter()
    private var listOfCountriesFromDB: MutableList<ModelItemDTO> = mutableListOf()
    private var base: DataBase? = null

    private val mViewModel: CountryListViewModel by stateViewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            param1 = it.getString(ARG_PARAM1)
            param2 = it.getString(ARG_PARAM2)
        }
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
        flag = if (flag) {
            menu.findItem(R.id.sort).setIcon(R.drawable.ic_down)
            false
        } else {
            menu.findItem(R.id.sort).setIcon(R.drawable.ic_up)
            true
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.sort ->
                flag = if (flag) {
                    mAdapter.sortDown()
                    item.setIcon(R.drawable.ic_down)
                    false
                } else {
                    mAdapter.sortUp()
                    item.setIcon(R.drawable.ic_up)
                    true
                }
            R.id.searchCountry -> { //идём на фильтр
                findNavController().navigate(R.id.filter_fragment)
                Toast.makeText(context, "抖音又一首摇头洗脑神曲", Toast.LENGTH_LONG).show()
            }
        }
        saveStanceSort()
        return super.onOptionsItemSelected(item)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        val view = inflater.inflate(R.layout.fragment_blank1, container, false)

        /*val callback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                findNavController().navigate(R.id.startFragment)
            }
        }

        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, callback)*/

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        loadStanceSort()
        recyclerView = view.findViewById(R.id.recyclerInFragment)

        recyclerView.adapter = mAdapter
        mAdapter.setItemClick { item ->
            val bundle = Bundle()
            bundle.putString(COUNTRY_NAME_KEY, item.name)
            findNavController().navigate(
                R.id.action_blankFragment1_to_countryDetailsFragment,
                bundle
            )
        }

        mViewModel.getAllCountryInfo()
        mViewModel.getLiveData().observe(viewLifecycleOwner, { outcome ->
            when (outcome) {
                is Outcome.Progress -> {
                }
                is Outcome.Next -> {
                    mAdapter.repopulate(outcome.data)
                    mViewModel.addAllCountryInfoInBD(outcome.data)
                    sort(outcome.data)

                    setFragmentResultListener("requestKey") { _, bundle ->
                        val resultFilter = bundle.getIntegerArrayList("filterPopulation")
                        mAdapter.repopulate(outcome.data.filter {
                            it.population in resultFilter!![0]..resultFilter[1]
                        } as MutableList<ModelItemDTO>)
                    }

                }
                is Outcome.Failure -> {
                }
                is Outcome.Success -> {
                }
            }
        })

        val countriesFromDB = base?.getInfoCountryDAO()?.getInfoCountries()
        val languagesFromDB = base?.getLanguagesDAO()
        listOfCountriesFromDB = countriesFromDB.convertDBData(
            languagesFromDB,
            listOfCountriesFromDB
        )
        sort(listOfCountriesFromDB)
    }

    private fun sort(list: MutableList<ModelItemDTO>) {
        mAdapter.repopulate(list)
        if (flag) {
            mAdapter.sortUp()
        } else {
            mAdapter.sortDown()
        }
        recyclerView.adapter = mAdapter
    }

    private fun saveStanceSort() {
        activity?.getSharedPreferences(FILE_NAME_SAVED_STANCE, Context.MODE_PRIVATE)
            ?.edit()
            ?.apply { putBoolean(SAVED_STANCE, flag) }
            ?.apply()
    }

    private fun loadStanceSort() {
        val sharPref = activity?.getSharedPreferences(FILE_NAME_SAVED_STANCE, Context.MODE_PRIVATE)
        val state = sharPref?.getBoolean(SAVED_STANCE, flag)
        if (state != null) {
            flag = state
        }
    }

}
