package com.example.homework.fragments.news

import com.example.domain.repository.NetworkRepositoryNews
import com.example.homework.base.adapter.mvi.BaseViewModel
import kotlinx.coroutines.flow.collect

class NewsViewModelMvi(private val repositoryNews: NetworkRepositoryNews) :
    BaseViewModel<NewsIntent, NewsAction, NewsState>() {

    override fun intentToAction(intent: NewsIntent): NewsAction {
        return when (intent) {
            is NewsIntent.LoadAllNews -> NewsAction.AllNews
        }
    }

    override fun handleAction(action: NewsAction) {
        launchOnUi {
            when (action) {
                is NewsAction.AllNews -> {

                    }
                }
            }
        }
    }
