package com.example.homework.fragments.filter

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.activity.OnBackPressedCallback
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.fragment.app.setFragmentResult
import androidx.lifecycle.SavedStateHandle
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import com.example.homework.R
import com.example.domain.outcome.Outcome
import com.example.homework.const.MAX_PEOPLE
import com.example.homework.const.MIN_PEOPLE
import com.example.domain.dto.ModelItemDTO
import com.google.android.material.slider.RangeSlider

class FilterFragment : Fragment() {

    private lateinit var mRangeSlider: RangeSlider
    private lateinit var minCountPeople: TextView
    private lateinit var maxCountPeople: TextView
    private lateinit var mButtonAccept: Button
    private lateinit var mModelItemDTO: MutableList<ModelItemDTO>
    private var mViewModule = FilterViewModule(SavedStateHandle())
    private var minPopulation: Int = 0
    private var maxPopulation: Int = 0

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        val view = inflater.inflate(R.layout.filter_fragment, container, false)

        /*val callback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                findNavController().navigate(R.id.blankFragment1)
            }
        }

        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, callback)*/

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        minCountPeople = view.findViewById(R.id.min_people_count)
        maxCountPeople = view.findViewById(R.id.max_people_count)
        mRangeSlider = view.findViewById(R.id.rangeSlider)
        mRangeSlider.valueFrom = MIN_PEOPLE
        mRangeSlider.valueTo = MAX_PEOPLE

        mViewModule.getAllCountryInfo()
        mViewModule.getLiveData().observe(viewLifecycleOwner, { outcome ->
            when (outcome) {
                is Outcome.Progress -> {
                }
                is Outcome.Next -> {
                    mModelItemDTO = outcome.data
                }
                is Outcome.Failure -> {
                }
                is Outcome.Success -> {
                }
            }
        })

        mButtonAccept = view.findViewById(R.id.buttonAcceptFilterFragment)
        mRangeSlider.addOnSliderTouchListener(object : RangeSlider.OnSliderTouchListener {
            override fun onStartTrackingTouch(slider: RangeSlider) {
                minPopulation = slider.values[0].toInt()
                maxPopulation = slider.values[1].toInt()
                minCountPeople.text = minPopulation.toString()
                maxCountPeople.text = maxPopulation.toString()
            }

            override fun onStopTrackingTouch(slider: RangeSlider) {
                minPopulation = slider.values[0].toInt()
                maxPopulation = slider.values[1].toInt()
                minCountPeople.text = minPopulation.toString()
                maxCountPeople.text = maxPopulation.toString()
            }

        })

        mButtonAccept.setOnClickListener {
            val mFilter = mutableListOf<Int>()
            mFilter.add(minPopulation)
            mFilter.add(maxPopulation)
            setFragmentResult("requestKey", bundleOf("filterPopulation" to mFilter))

            Navigation.findNavController(view)
                .navigate(R.id.blankFragment1)
        }

        super.onViewCreated(view, savedInstanceState)
    }

}