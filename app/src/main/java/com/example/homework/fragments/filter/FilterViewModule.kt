package com.example.homework.fragments.filter

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import com.example.homework.base.adapter.mvvm.BaseViewModel
import com.example.domain.outcome.Outcome
import com.example.homework.base.adapter.mvvm.executeJob
import com.example.domain.dto.ModelItemDTO

class FilterViewModule(savedStateHandle: SavedStateHandle) : BaseViewModel(savedStateHandle) {

    private val mFilterCountryLiveData =
        savedStateHandle.getLiveData<Outcome<MutableList<ModelItemDTO>>>("countryInfo")

    fun getLiveData(): MutableLiveData<Outcome<MutableList<ModelItemDTO>>> {
        return mFilterCountryLiveData
    }

    fun getAllCountryInfo() {
        mCompositeDisposable.add(
            executeJob(
                com.example.data.network.RetrofitCreator().getRetrofit().getNameList(),
                mFilterCountryLiveData
            )
        )
    }

}