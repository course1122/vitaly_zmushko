package com.example.homework.fragments.startFragment

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.homework.R
import com.example.homework.services.LocationService

class StartFragment : Fragment() {

    private lateinit var buttonAllCountryInfo: Button
    private lateinit var buttonFragmentWithCoroutines: Button
    private lateinit var buttonMapFragment: Button
    private lateinit var buttonNews: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val intentFilter = IntentFilter()
        intentFilter.addAction(LocationService.NEW_LOCATION_ACTION)
        context?.registerReceiver(mLocationBroadcastReceiver, intentFilter)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.start_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        buttonAllCountryInfo = view.findViewById(R.id.buttonAllCountryInfo)
        buttonFragmentWithCoroutines = view.findViewById(R.id.buttonFragmentWithCoroutines)
        buttonNews = view.findViewById(R.id.button_news)

        buttonAllCountryInfo.setOnClickListener { findNavController().navigate(R.id.blankFragment1) }
        buttonFragmentWithCoroutines.setOnClickListener { findNavController().navigate(R.id.fragmentCountryInfoWithCoroutines) }
//      buttonMapFragment.setOnClickListener { findNavController().navigate(R.id.mapFragment) }
        buttonNews.setOnClickListener { findNavController().navigate(R.id.newsFragment) }


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            this.context?.startForegroundService(
                Intent(
                    this.context,
                    LocationService::class.java
                )
            )
        } else {
            this.context?.startService(
                Intent(
                    this.context,
                    LocationService::class.java
                )
            )
        }


    }


    override fun onDestroy() {
        super.onDestroy()
        context?.unregisterReceiver(mLocationBroadcastReceiver)
    }


}
private val mLocationBroadcastReceiver: BroadcastReceiver = object : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent?) {
        if (intent != null && intent.action != null) {
            when (intent.action) {
                LocationService.NEW_LOCATION_ACTION -> {
                    //locationPublisher.onNext(location)
                    Log.e("hz", intent.getDoubleExtra("lat", 0.0).toString())
                }
            }
        }
    }
}
