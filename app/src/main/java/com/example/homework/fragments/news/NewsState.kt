package com.example.homework.fragments.news

import com.example.domain.dto.ArticleDTO
import com.example.homework.base.adapter.mvi.ViewState

sealed class NewsState : ViewState {
        data class Loading(val loading : Boolean) : NewsState()
        data class ResultAllNews(val data : List<ArticleDTO>): NewsState()
        data class Exception(val callErrors: Throwable) : NewsState()
}