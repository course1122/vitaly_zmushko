package com.example.homework.fragments.countrydetails

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.Toast
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import coil.ImageLoader
import coil.decode.SvgDecoder
import coil.request.ImageRequest
import com.example.domain.dto.ModelItemDTO
import com.example.homework.R
import com.example.homework.adapters.LanguageAdapter
import com.example.homework.base.adapter.mvp.BaseMvpFragment
import com.example.homework.bundle.COUNTRY_NAME_KEY
import com.example.homework.const.ERROR
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.MapView
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions

class CountryDetailsFragment : BaseMvpFragment<CountryDetailsView, CountryDetailsPresenter>(),
    OnMapReadyCallback,
    CountryDetailsView {

    private lateinit var mRvLanguage: RecyclerView
    private lateinit var tvCountryName: AppCompatTextView
    private lateinit var mMapsView: MapView
    private lateinit var mIvFlagURL: ImageView
    private lateinit var mGoogleMap: GoogleMap
    private lateinit var mLanguageAdapter: LanguageAdapter
    private lateinit var mProgress: ProgressBar

    private lateinit var mRefresh: SwipeRefreshLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getPresenter().mCountryName = arguments?.getString(COUNTRY_NAME_KEY) ?: ERROR
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        val view = inflater.inflate(R.layout.fragment_country_details, container, false)
        getPresenter().mCountryName = arguments?.getString(COUNTRY_NAME_KEY).toString()
        mMapsView = view?.findViewById(R.id.google_map)!!
        mMapsView.onCreate(savedInstanceState)

        /*val callback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                findNavController().navigate(R.id.blankFragment1)
            }
        }

        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, callback)*/

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getPresenter().attachView(this)
        mRvLanguage = view.findViewById(R.id.recyclerLanguage)
        tvCountryName = view.findViewById(R.id.countryName)
        mIvFlagURL = view.findViewById(R.id.countryImageFlag)
        mProgress = view.findViewById(R.id.progress)
        mRefresh = view.findViewById(R.id.swipeRefresh)

        mMapsView.getMapAsync {
            mGoogleMap = it
        }

        tvCountryName.text = getPresenter().mCountryName
        mLanguageAdapter = LanguageAdapter()
        mRvLanguage.adapter = mLanguageAdapter

        mRefresh.setOnRefreshListener {
            getPresenter().getItemByCountryName(getPresenter().mCountryName, true)
        }
        //getPresenter().getItemByCountryName(mCountryName, false) //убрать коммент штоб не тестить
    }

    override fun onMapReady(googleMap: GoogleMap) {
        googleMap.addMarker(
            MarkerOptions()
                .position(LatLng(0.0, 0.0))
                .title("Marker")
        )
    }

    override fun onResume() {
        mMapsView.onResume()
        super.onResume()
    }

    override fun onDestroy() {
        super.onDestroy()
        mMapsView.onDestroy()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mMapsView.onLowMemory()
    }

    override fun showCountryInfo(response: List<ModelItemDTO>) {
        response[0].languages.let { mLanguageAdapter.addList(it) }
        mIvFlagURL.loadSvg(response[0].flag)
        val position = LatLng(
            response[0].latlng[0],
            response[0].latlng[1]
        )
        mGoogleMap.addMarker(
            MarkerOptions()
                .position(position)
        )
        val cameraPosition = CameraPosition.Builder()
            .target(position).build()
        mGoogleMap.animateCamera(
            CameraUpdateFactory.newCameraPosition(
                cameraPosition
            )
        )
        mRefresh.isRefreshing = false
    }

    override fun showError(error: String, throwable: Throwable) {
        Toast.makeText(context, error, Toast.LENGTH_LONG).show()
    }

    override fun showProgress() {
        mProgress.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        mProgress.visibility = View.GONE
    }

    override fun createPresenter() {
        mPresenter = CountryDetailsPresenter()
    }

    override fun getPresenter(): CountryDetailsPresenter = mPresenter

    //для работы картинок coil svg
    private fun ImageView.loadSvg(url: String) {
        val imageLoader = ImageLoader.Builder(this.context)
            .componentRegistry { add(SvgDecoder(this@loadSvg.context)) }
            .build()

        val request = ImageRequest.Builder(this.context)
            .data(url)
            .target(this)
            .build()

        imageLoader.enqueue(request)
    }

}