package com.example.homework.fragments.countrydetails

import com.example.homework.base.adapter.mvp.BaseMvpView
import com.example.domain.dto.ModelItemDTO

interface CountryDetailsView : BaseMvpView {

    fun showCountryInfo(response: List<ModelItemDTO>)

}
