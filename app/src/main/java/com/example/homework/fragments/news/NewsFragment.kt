package com.example.homework.fragments.news

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.asLiveData
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.RecyclerView
import com.example.domain.dto.ArticleDTO
import com.example.domain.outcome.Outcome
import com.example.homework.R
import com.example.homework.adapters.NewsLocationRecyclerAdapter
import org.koin.androidx.scope.ScopeFragment
import org.koin.androidx.viewmodel.ext.android.stateViewModel

class NewsFragment : ScopeFragment() {

    private lateinit var recyclerViewNews: RecyclerView

    private var mAdapterNewsFragment = NewsLocationRecyclerAdapter()

    private val mNewsModelCapital: NewsFragmentViewModel by stateViewModel()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        val view =
            inflater.inflate(R.layout.news_fragment, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        recyclerViewNews = view.findViewById(R.id.recyclerNews)

        recyclerViewNews.adapter = mAdapterNewsFragment

        mNewsModelCapital.getNews().asLiveData(lifecycleScope.coroutineContext)
            .observe(viewLifecycleOwner, { outcome ->
                when (outcome) {
                    is Outcome.Failure -> {
                        Log.e("sad", outcome.e.message.toString())
                    }
                    is Outcome.Next -> {
                    }
                    is Outcome.Progress -> {
                    }
                    is Outcome.Success -> {
                        mAdapterNewsFragment.repopulate(outcome.data as MutableList<ArticleDTO>)
                    }
                }
            })


        /*mNewsModelCapital.getLiveData().observe(viewLifecycleOwner, { outcome ->
            when (outcome) {
                is Outcome.Progress -> {
                }
                is Outcome.Next -> {
                }
                is Outcome.Failure -> {
                }
                is Outcome.Success -> {
                    mAdapterNewsFragment.repopulate(outcome.data as MutableList<ArticleDTO>)
                }
            }
        })*/


    }

}
