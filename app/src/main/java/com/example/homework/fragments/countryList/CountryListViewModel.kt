package com.example.homework.fragments.countryList

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import com.example.homework.base.adapter.mvvm.BaseViewModel
import com.example.domain.outcome.Outcome
import com.example.homework.base.adapter.mvvm.executeJob
import com.example.homework.database.DataBase
import com.example.homework.database.TableCountry
import com.example.homework.database.TableLanguage
import com.example.domain.dto.ModelItemDTO
import com.example.homework.repository.database.DatabaseRepository

class CountryListViewModel (
    savedStateHandle: SavedStateHandle,
    mDataBase: DataBase,
    private val mDatabaseRepository: DatabaseRepository
) : BaseViewModel(savedStateHandle) {

    private val daoCountry = mDataBase.getInfoCountryDAO()
    private val daoLanguage = mDataBase.getLanguagesDAO()

    private val mCountryLiveData =
        savedStateHandle.getLiveData<Outcome<MutableList<ModelItemDTO>>>("countryItemDTO")

    private val mCountryLiveDataBD =
        savedStateHandle.getLiveData<Outcome<MutableList<ModelItemDTO>>>("countryItemDTOBD")

    fun getLiveData(): MutableLiveData<Outcome<MutableList<ModelItemDTO>>> {
        return mCountryLiveData
    }

    fun getLiveDataBD(): MutableLiveData<Outcome<MutableList<ModelItemDTO>>> {
        return mCountryLiveDataBD
    }

    fun getAllCountryInfo() {
        mCompositeDisposable.add(
            executeJob(
                com.example.data.network.RetrofitCreator().getRetrofit().getNameList(),
                mCountryLiveData
            )
        )
    }

    fun addAllCountryInfoInBD(response: MutableList<ModelItemDTO>) {
        val listCountry: MutableList<TableCountry> = mutableListOf()
        val listLanguages: MutableList<TableLanguage> = mutableListOf()
        response.let {
            response.forEach { item ->
                listCountry.add(
                    TableCountry(
                        item.name,
                        item.capital,
                        item.population
                    )
                )
                item.languages.forEach { language ->
                    daoLanguage.insertLanguage(
                        TableLanguage(
                            item.name,
                            language.name
                        )
                    )
                }
            }
            daoCountry.addAll(listCountry)
            daoLanguage.addAll(listLanguages)
        }
    }

}