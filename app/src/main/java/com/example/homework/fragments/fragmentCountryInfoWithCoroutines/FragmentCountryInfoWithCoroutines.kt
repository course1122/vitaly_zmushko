package com.example.homework.fragments.fragmentCountryInfoWithCoroutines

import android.os.Bundle
import android.view.*
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.asLiveData
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.domain.dto.ModelItemDTO
import com.example.domain.outcome.Outcome
import com.example.homework.R
import com.example.homework.adapters.RecyclerAdapterCoroutinesFragment
import kotlinx.coroutines.Job
import org.koin.androidx.scope.ScopeFragment
import org.koin.androidx.viewmodel.ext.android.stateViewModel

class FragmentCountryInfoWithCoroutines : ScopeFragment() {

    private lateinit var recyclerViewCoroutinesFragment: RecyclerView

    private var mAdapterCoroutinesFragment = RecyclerAdapterCoroutinesFragment()

    private val mViewModelCapital: CapitalCountriesFragmentViewModel by stateViewModel()

    private lateinit var mFlowJob: Job


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        val view =
            inflater.inflate(R.layout.fragment_country_info_with_coroutines, container, false)

        /*val callback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                findNavController().navigate(R.id.startFragment)
            }
        }

        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, callback)*/

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        recyclerViewCoroutinesFragment = view.findViewById(R.id.recyclerWithCapitals)

        mViewModelCapital.getSearchSubjectFlow()

        recyclerViewCoroutinesFragment.adapter = mAdapterCoroutinesFragment

        mViewModelCapital.getAllCapitals()
        mViewModelCapital.getLiveData().observe(viewLifecycleOwner, { outcome ->
            when (outcome) {
                is Outcome.Progress -> {
                }
                is Outcome.Next -> {
                }
                is Outcome.Failure -> {
                }
                is Outcome.Success -> {
                    mAdapterCoroutinesFragment.repopulate(outcome.data as MutableList<ModelItemDTO>)
                }
            }
        })

        mViewModelCapital.getAllCapitalsFlow().asLiveData(lifecycleScope.coroutineContext)
            .observe(viewLifecycleOwner, {
                when (it) {
                    is Outcome.Failure -> {
                    }
                    is Outcome.Next -> {
                    }
                    is Outcome.Progress -> {
                    }
                    is Outcome.Success -> {
                        mAdapterCoroutinesFragment.repopulate(it.data as MutableList<ModelItemDTO>)
                    }
                }
            })

    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.search_action, menu)
        super.onCreateOptionsMenu(menu, inflater)
        val menuItem: MenuItem = menu.findItem(R.id.search_button_capitals)
        val searchView: SearchView = menuItem.actionView as SearchView
        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                query?.let {
                    mViewModelCapital.mSearchStateFlow.value = query
                }
                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {
                mViewModelCapital.mSearchStateFlow.value = newText
                return false
            }
        })
        searchView.setOnCloseListener {
            mViewModelCapital.getAllCapitalsFlow().asLiveData(lifecycleScope.coroutineContext)
                .observe(viewLifecycleOwner, {
                    when (it) {
                        is Outcome.Failure -> {
                        }
                        is Outcome.Next -> {
                        }
                        is Outcome.Progress -> {
                        }
                        is Outcome.Success -> {
                            mAdapterCoroutinesFragment.repopulate(it.data as MutableList<ModelItemDTO>)
                        }
                    }
                })
            false
        }
    }

}