package com.example.homework.fragments.fragmentCountryInfoWithCoroutines

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.example.domain.dto.ModelItemDTO
import com.example.domain.outcome.Outcome
import com.example.domain.repository.NetworkCapitalsRepositoryFlow
import com.example.homework.base.adapter.mvvm.BaseViewModel
import com.example.homework.const.MILLISECOND
import com.example.homework.const.MIN_SYMBOLS_ON_SEARCH
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class CapitalCountriesFragmentViewModel(
    savedStateHandle: SavedStateHandle,
    private val networkRepositoryWithCoroutines: com.example.domain.repository.NetworkRepositoryWithCoroutines,
    private val mNetworkCapitalsRepositoryFlow: NetworkCapitalsRepositoryFlow,
) : BaseViewModel(savedStateHandle) {

    var mSearchStateFlow = MutableStateFlow("")

    private val mCountryCapitalsLiveData =
        savedStateHandle.getLiveData<Outcome<List<ModelItemDTO>>>("countryCapitalsItemDTO")

    fun getLiveData(): MutableLiveData<Outcome<List<ModelItemDTO>>> {
        return mCountryCapitalsLiveData
    }

    fun getAllCapitals() {
        CoroutineScope(viewModelScope.coroutineContext).launch {
            try {
                mCountryCapitalsLiveData.value = Outcome.loading(true)
                val result = withContext(viewModelScope.coroutineContext + Dispatchers.IO) {
                    //networkRepositoryWithCoroutines.getCapitalsListAsync()
                    com.example.data.network.CoroutinesRetrofitCreator().getCoroutinesRetrofit()
                        .getCapitalListAsync()
                }
                mCountryCapitalsLiveData.value = Outcome.loading(false)
                mCountryCapitalsLiveData.value = Outcome.success(result)
            } catch (e: Exception) {
                mCountryCapitalsLiveData.value = Outcome.loading(false)
                mCountryCapitalsLiveData.value = Outcome.failure(e)
            }
        }
    }

    fun getAllCapitalsFlow(): Flow<Outcome<List<ModelItemDTO>>> =
        mNetworkCapitalsRepositoryFlow.getCapitalsList()

    fun getSearchSubjectFlow() {
        viewModelScope.launch {
            mSearchStateFlow
                .filter { it.length >= MIN_SYMBOLS_ON_SEARCH }
                .debounce(MILLISECOND)
                .distinctUntilChanged()
                .map { it.lowercase() }
                .flatMapLatest {
                    mNetworkCapitalsRepositoryFlow.getCapitalByName(it)
                }
                .flowOn(Dispatchers.IO)
                .collect {
                    mCountryCapitalsLiveData.value = it
                }
        }
    }

}