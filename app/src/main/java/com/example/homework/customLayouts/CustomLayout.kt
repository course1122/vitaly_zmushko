package com.example.homework.customLayouts

import android.content.Context
import android.util.AttributeSet
import android.widget.LinearLayout
import android.widget.TextView
import com.example.homework.R

class CustomLayout : LinearLayout {

    private var mLikes: TextView? = null
    private var mLikesId: Int? = null
    private var mComments: TextView? = null
    private var mCommentsId: Int? = null
    private var mDistance: TextView? = null
    private var mDistanceId: Int? = null


    constructor(context: Context) : super(context) {
        initView(context, null)
    }

    constructor(context: Context, att: AttributeSet) : super(context, att) {
        initView(context, att)
    }

    constructor(context: Context, att: AttributeSet, defStyleAttr: Int) : super(context,
        att,
        defStyleAttr) {
        initView(context, att)
    }

    private fun initView(context: Context, att: AttributeSet?) {
        val view = inflate(context, R.layout.custom_view, this)

        layoutParams = LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT)

        mLikes = view.findViewById(R.id.likes_view)
        mComments = view.findViewById(R.id.comment_text)
        mDistance = view.findViewById(R.id.distance_view)

        att?.let {
            val typedArray = context.theme.obtainStyledAttributes(
                att, R.styleable.CustomLayout, 0, 0
            )
            try {
                mLikesId =
                    typedArray.getResourceId(R.styleable.CustomLayout_likeText, -1)
                mLikesId?.let { mLikes?.text = "111" }

                mCommentsId =
                    typedArray.getResourceId(R.styleable.CustomLayout_commentText, -1)
                mCommentsId?.let { mComments?.text = "111" }

                mDistanceId =
                    typedArray.getResourceId(R.styleable.CustomLayout_distanceText, -1)
                mDistanceId?.let { mDistance?.text = "111" }

            } catch (e: Exception) {

            } finally {
                typedArray.recycle()
            }


        }

    }
}