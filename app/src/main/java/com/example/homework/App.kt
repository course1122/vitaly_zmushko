package com.example.homework

import android.app.Application
import com.example.homework.di.appModule
import com.example.homework.di.countryListCapitalsModule
import com.example.homework.di.countryListModule
import com.example.homework.di.newsModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.GlobalContext.startKoin

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            // Koin Android logger
            androidLogger()
            //inject Android context
            androidContext(this@App)
            // use modules
            modules(
                appModule,
                countryListModule,
                countryListCapitalsModule,
                newsModule
            )
        }
    }

}