package com.example.homework.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.example.homework.R
import com.example.homework.base.adapter.adapter.BaseAdapter
import com.example.homework.ext.rebuildListLanguages
import com.example.domain.dto.ModelItemDTO
import com.example.homework.customLayouts.CustomLayout

class RecyclerAdapter : BaseAdapter<ModelItemDTO>() {

    class InfoCountryViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val countryName: AppCompatTextView = itemView.findViewById(R.id.countryName)
        val countryCapital: AppCompatTextView = itemView.findViewById(R.id.countryCapital)
        val countryLanguages: AppCompatTextView = itemView.findViewById(R.id.countryLanguages)
        val countPeoples: AppCompatTextView = itemView.findViewById(R.id.countPeoplesInCountry)
        val mCustomLayout: CustomLayout = itemView.findViewById(R.id.my_custom_layout)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): InfoCountryViewHolder {
        val itemView =
            LayoutInflater.from(parent.context).inflate(R.layout.recyclerviev, parent, false)
        return InfoCountryViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is InfoCountryViewHolder) {
            val item = mDataList[position]
            holder.countryName.text = item.name
            holder.countryCapital.text = item.capital
            holder.countryLanguages.text = item.languages.rebuildListLanguages()
            holder.countPeoples.text = item.population.toString()
            holder.itemView.setOnClickListener { mOnItemClickListener?.invoke(item) }
        }
    }

    fun sortUp() {
        mDataList.sortBy { it.population }
        notifyDataSetChanged()
    }

    fun sortDown() {
        mDataList.sortByDescending { it.population }
        notifyDataSetChanged()
    }

    fun clear(){
        mDataList.clear()
        notifyDataSetChanged()
    }

}