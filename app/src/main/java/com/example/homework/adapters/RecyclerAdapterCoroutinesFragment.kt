package com.example.homework.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.example.homework.R
import com.example.homework.base.adapter.adapter.BaseAdapter
import com.example.domain.dto.ModelItemDTO

class RecyclerAdapterCoroutinesFragment : BaseAdapter<ModelItemDTO>() {

    class InfoCountryCapitalViewHolderCoroutines(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val countryCapital: AppCompatTextView = itemView.findViewById(R.id.textViewCountryCapitalCoroutinesFragment)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): InfoCountryCapitalViewHolderCoroutines {
        val itemView =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.recycler_view_coroutines_fragment, parent, false)
        return InfoCountryCapitalViewHolderCoroutines(itemView)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is InfoCountryCapitalViewHolderCoroutines) {
            val item = mDataList[position]
            holder.countryCapital.text = item.capital
        }
    }

    fun clear(){
        mDataList.clear()
        notifyDataSetChanged()
    }

}