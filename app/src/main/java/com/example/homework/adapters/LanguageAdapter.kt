package com.example.homework.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.example.homework.R
import com.example.homework.base.adapter.adapter.BaseAdapter
import com.example.domain.dto.LanguageDTO

class LanguageAdapter : BaseAdapter<LanguageDTO>() {

    class LanguageViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val mLanguageDTO: AppCompatTextView = itemView.findViewById(R.id.tv_language)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val itemView =
            LayoutInflater.from(parent.context).inflate(R.layout.item_language, parent, false)
        return LanguageViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is LanguageViewHolder) {
            val item = mDataList[position]
            holder.mLanguageDTO.text = item.name
        }
    }
}