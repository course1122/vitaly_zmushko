package com.example.homework.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.example.domain.dto.ArticleDTO
import com.example.homework.R
import com.example.homework.base.adapter.adapter.BaseAdapter

class NewsLocationRecyclerAdapter : BaseAdapter<ArticleDTO>() {

    class NewsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val title: AppCompatTextView = itemView.findViewById(R.id.text_view_title)
        val description: AppCompatTextView = itemView.findViewById(R.id.text_view_description)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NewsViewHolder {
        val itemView =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.recycler_news_location, parent, false)
        return NewsViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is NewsViewHolder) {
            val item = mDataList[position]
            holder.title.text = item.title
            holder.description.text = item.description
        }

        fun clear() {
            mDataList.clear()
            notifyDataSetChanged()
        }

    }
}