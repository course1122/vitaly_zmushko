package com.example.homework.database

import androidx.room.ColumnInfo
import androidx.room.Entity

@Entity(tableName = "TableLanguage", primaryKeys = ["nameCountry", "languages"])
class TableLanguage(

    @ColumnInfo var nameCountry: String,
    @ColumnInfo var languages: String
)