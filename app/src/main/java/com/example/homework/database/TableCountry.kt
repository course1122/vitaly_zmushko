package com.example.homework.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "TableCountry")
class TableCountry(

    @PrimaryKey @ColumnInfo val name: String,
    @ColumnInfo val capital: String,
    @ColumnInfo val population: Int

)
