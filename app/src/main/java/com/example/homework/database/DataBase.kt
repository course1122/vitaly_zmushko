package com.example.homework.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

@Database(entities = [TableCountry::class, TableLanguage::class], version = 1)
abstract class DataBase : RoomDatabase() {

    abstract fun getInfoCountryDAO(): InfoCountryDAO
    abstract fun getLanguagesDAO(): LanguagesDAO

    companion object {
        fun init(context: Context) = Room
            .databaseBuilder(context, DataBase::class.java, "CountriesDataBase")
            .allowMainThreadQueries()
            .build()
    }

}