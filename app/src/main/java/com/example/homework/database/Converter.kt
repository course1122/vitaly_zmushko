package com.example.homework.database

import com.example.domain.dto.LanguageDTO
import com.example.domain.dto.ModelItemDTO

fun List<TableCountry>?.convertDBData(
    languagesFromDB: LanguagesDAO?, listOfCountriesFromDB: MutableList<ModelItemDTO>
): MutableList<ModelItemDTO> {
    this?.forEach { countryDB ->
        val listOfLanguagesFromDB: MutableList<LanguageDTO> = mutableListOf()
        languagesFromDB?.getLanguagesInCountryName(countryDB.name)?.forEach { languageDB ->
            val languageItem = LanguageDTO(
                languageDB
            )
            listOfLanguagesFromDB.add(languageItem)
        }
        val countryDataItem = ModelItemDTO(
            countryDB.name,
            countryDB.capital,
            countryDB.population,
            listOfLanguagesFromDB
        )
        listOfCountriesFromDB.add(countryDataItem)
    }
    return listOfCountriesFromDB
}