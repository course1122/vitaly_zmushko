package com.example.homework.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface LanguagesDAO {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertLanguage(entity: TableLanguage)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun addAll(list: List<TableLanguage>)

    @Query("SELECT * FROM TableLanguage")
    fun getLanguagesInAllCountries(): List<TableLanguage>

    @Query("SELECT languages FROM TableLanguage WHERE nameCountry = :name")
    fun getLanguagesInCountryName(name: String): List<String>
}