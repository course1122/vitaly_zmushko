package com.example.homework.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

@Dao
interface InfoCountryDAO {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertData(entity: TableCountry)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun addAll(list: List<TableCountry>)

    @Query("SELECT * FROM TableCountry")
    fun getInfoCountries(): List<TableCountry>

    @Query("SELECT * FROM TableCountry WHERE name = :name")
    fun getInfoCountriesName(name: String): List<TableCountry>


}