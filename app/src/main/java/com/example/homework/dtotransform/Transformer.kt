package com.example.homework.dtotransform

interface Transformer <InputType, OutputType> {
    fun transform(item: InputType?) : OutputType
}