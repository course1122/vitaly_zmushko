package com.example.homework.dtotransform

import com.example.data.model.Language
import com.example.domain.dto.LanguageDTO

class LanguageTransformToLanguageDTO : Transformer<Language, LanguageDTO> {

    override fun transform(item: Language?): LanguageDTO {
        val languageDTO = LanguageDTO()
        item?.let {
            languageDTO.name = it.name ?: ""
        }
        return languageDTO
    }

}