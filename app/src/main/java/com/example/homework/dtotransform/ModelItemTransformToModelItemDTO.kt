package com.example.homework.dtotransform

import com.example.domain.dto.LanguageDTO
import com.example.data.model.ModelItem
import com.example.domain.dto.ModelItemDTO

class ModelItemTransformToModelItemDTO : Transformer<ModelItem, ModelItemDTO> {

    override fun transform(item: ModelItem?): ModelItemDTO {
        val modelItemDTO = ModelItemDTO()
        val listLanguageDTO: MutableList<LanguageDTO> = mutableListOf()
        item?.let {
            modelItemDTO.name = it.name ?: ""
            modelItemDTO.flag = it.flag ?: ""
            modelItemDTO.population = it.population ?: -1
            modelItemDTO.capital = it.capital ?: ""
            it.languages?.forEach { _ ->
                val languageDTO = LanguageDTO()
                languageDTO.name = it.name ?: ""
                listLanguageDTO.add(languageDTO)
            }
            modelItemDTO.languages = listLanguageDTO
            modelItemDTO.latlng = it.latlng ?: arrayListOf(1.0, 1.0)
        }
        return modelItemDTO
    }

    fun MutableList<ModelItem>?.convertToDTO(): MutableList<ModelItemDTO> {
        val modelItemDTO = ModelItemDTO()
        val listLanguageDTO: MutableList<LanguageDTO> = mutableListOf()
        this?.forEach {
            modelItemDTO.name = it.name ?: ""
            modelItemDTO.flag = it.flag ?: ""
            modelItemDTO.population = it.population ?: -1
            modelItemDTO.capital = it.capital ?: ""
            it.languages?.forEach { _ ->
                val languageDTO = LanguageDTO()
                languageDTO.name = it.name ?: ""
                listLanguageDTO.add(languageDTO)
            }
            modelItemDTO.languages = listLanguageDTO
            modelItemDTO.latlng = it.latlng ?: arrayListOf(1.0, 1.0)
        }
        return mutableListOf(modelItemDTO)
    }

}