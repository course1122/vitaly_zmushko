package com.example.homework.dtotransform

import com.example.data.newsModel.Article
import com.example.data.newsModel.NewsApiModel
import com.example.domain.dto.ArticleDTO
import com.example.domain.outcome.Transformer

class NewsApiModelToNewsApiModelDTO : Transformer<NewsApiModel, List<ArticleDTO>> {

    override var convert: (NewsApiModel) -> List<ArticleDTO> =
        { data ->
            data.articles.map {
                ArticleDTO(
                    title = it.title ?: "",
                    description = it.description ?: "")
            }
        }

}