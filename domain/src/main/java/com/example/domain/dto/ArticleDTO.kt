package com.example.domain.dto

data class ArticleDTO(
    val title: String = "",
    val description: String = "",
)