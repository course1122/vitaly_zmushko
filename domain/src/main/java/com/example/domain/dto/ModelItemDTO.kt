package com.example.domain.dto

class ModelItemDTO(
    var capital: String = "",
    var name: String = "",
    var population: Int = 0,
    var languages: MutableList<LanguageDTO> = mutableListOf(),
    var flag: String = "",
    var latlng: List<Double> = arrayListOf(1.0, 1.0)
)