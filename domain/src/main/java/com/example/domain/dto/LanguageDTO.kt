package com.example.domain.dto

class LanguageDTO(
    var name: String = ""
) {
    override fun toString(): String {
        return name
    }
}