package com.example.domain.repository

import com.example.domain.dto.ModelItemDTO
import com.example.domain.outcome.Outcome
import kotlinx.coroutines.flow.Flow

interface NetworkCapitalsRepositoryFlow {

    fun getCapitalsList(): Flow<Outcome<List<ModelItemDTO>>>

    fun getCapitalByName(capitalName: String): Flow<Outcome<List<ModelItemDTO>>>

}