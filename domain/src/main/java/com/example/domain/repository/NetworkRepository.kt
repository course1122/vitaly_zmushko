package com.example.domain.repository

import com.example.domain.dto.ModelItemDTO
import io.reactivex.rxjava3.core.Flowable

interface NetworkRepository {

    fun getNameList(): Flowable<MutableList<ModelItemDTO>>

    fun getCountryByName(name: String): Flowable<MutableList<ModelItemDTO>>

}