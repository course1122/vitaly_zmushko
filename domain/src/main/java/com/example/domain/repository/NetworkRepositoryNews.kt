package com.example.domain.repository

import com.example.domain.dto.ArticleDTO
import com.example.domain.outcome.Outcome
import kotlinx.coroutines.flow.Flow

interface NetworkRepositoryNews {

    fun getNews(codeCountry: String): Flow<Outcome<List<ArticleDTO>>>

}