package com.example.domain.repository

import com.example.domain.dto.ModelItemDTO

interface NetworkRepositoryWithCoroutines {

    suspend fun getCapitalsListAsync(): MutableList<ModelItemDTO>

}