package com.example.data

//Network const
const val GET_ALL_INFO_COUNTRIES = "all"
const val GET_ALL_CAPITAL = "all?fields=capital"
const val BASE_URL = "https://restcountries.eu/rest/v2/"
const val PATH_VARIABLE = "path"
const val NAME_PATH_URL = "/{$PATH_VARIABLE}"
const val GET_COUNTRY_BY_NAME = "name$NAME_PATH_URL"
const val GET_CAPITAL_BY_NAME = "capital$NAME_PATH_URL"

//https://newsapi.org/v2/top-headlines?country=ru&apiKey=ff1657fa85bf4212ba63b786d57d5193

const val BASE_URL_NEWS = "https://newsapi.org/v2/"
const val API_KEY_NEWS = "X-Api-Key: ff1657fa85bf4212ba63b786d57d5193"
const val GET_ALL_NEWS = "everything?q=Apple&from=2021-09-13&sortBy=popularity&apiKey=$API_KEY_NEWS"
const val GET_NEWS_BY_COUNTRY = "top-headlines"
const val COUNTRY = "country"

const val SESSION_TIMEOUT: Long = 10000