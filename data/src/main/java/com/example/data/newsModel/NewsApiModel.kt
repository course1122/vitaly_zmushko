package com.example.data.newsModel

data class NewsApiModel(
    val articles: List<Article>,
    val status: String,
    val totalResults: Int
)