package com.example.data.ext

import com.example.data.model.ModelItem
import com.example.domain.dto.ModelItemDTO
import com.example.domain.outcome.Transformer

class ListCapitalsToListCapitalsDto :
    Transformer<List<ModelItem>, List<ModelItemDTO>> {

    override var convert: (List<ModelItem>) -> List<ModelItemDTO> =
        { data ->
            data.map { ModelItemDTO(capital = it.capital ?: "") }
        }
}

