package com.example.data.network

import com.example.data.GET_ALL_INFO_COUNTRIES
import com.example.data.GET_CAPITAL_BY_NAME
import com.example.data.PATH_VARIABLE
import com.example.data.model.ModelItem
import kotlinx.coroutines.flow.Flow
import retrofit2.http.GET
import retrofit2.http.Path

interface FlowRetrofitService {

    @GET(GET_ALL_INFO_COUNTRIES)
    fun getNameList(): Flow<MutableList<ModelItem>>

    @GET(GET_CAPITAL_BY_NAME)
    fun getCountryByName(@Path(PATH_VARIABLE) name: String): Flow<List<ModelItem>>

}