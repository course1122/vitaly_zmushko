package com.example.data.network

import com.example.data.API_KEY_NEWS
import com.example.data.COUNTRY
import com.example.data.GET_NEWS_BY_COUNTRY
import com.example.data.newsModel.Article
import com.example.data.newsModel.NewsApiModel
import kotlinx.coroutines.flow.Flow
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface NewsRetrofitService {

    @Headers(API_KEY_NEWS)
    @GET(GET_NEWS_BY_COUNTRY)
    fun getNewsByCountry(@Query(COUNTRY) countryNamed: String = "ru"): Flow<NewsApiModel>

}