package com.example.data.network

import com.example.data.GET_ALL_INFO_COUNTRIES
import com.example.data.GET_COUNTRY_BY_NAME
import com.example.data.PATH_VARIABLE
import com.example.domain.dto.ModelItemDTO
import io.reactivex.rxjava3.core.Flowable
import retrofit2.http.GET
import retrofit2.http.Path

interface RetrofitService {

    @GET(GET_ALL_INFO_COUNTRIES)
    fun getNameList(): Flowable<MutableList<ModelItemDTO>>

    @GET(GET_COUNTRY_BY_NAME)
    fun getCountryByName(@Path(PATH_VARIABLE) name: String): Flowable<MutableList<ModelItemDTO>>

}