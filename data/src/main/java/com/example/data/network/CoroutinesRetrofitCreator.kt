package com.example.data.network

import com.example.data.BASE_URL
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class CoroutinesRetrofitCreator {

    fun getCoroutinesRetrofit(): RetrofitCoroutinesService {
        return Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(CoroutineCallAdapterFactory.invoke())
            .baseUrl(BASE_URL)
            .build()
            .create(RetrofitCoroutinesService::class.java)
    }

}