package com.example.data.network

import com.chenxyu.retrofit.adapter.FlowCallAdapterFactory
import com.example.data.BASE_URL
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class FlowRetrofitCreator {

    fun getFlowCoroutinesRetrofit(): FlowRetrofitService {
        return Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(FlowCallAdapterFactory())
            .baseUrl(BASE_URL)
            .build()
            .create(FlowRetrofitService::class.java)
    }

}