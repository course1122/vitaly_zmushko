package com.example.data.network

import com.example.data.GET_ALL_CAPITAL
import com.example.domain.dto.ModelItemDTO
import retrofit2.http.GET

interface RetrofitCoroutinesService {

    @GET(GET_ALL_CAPITAL)
    suspend fun getCapitalListAsync(): MutableList<ModelItemDTO>
}