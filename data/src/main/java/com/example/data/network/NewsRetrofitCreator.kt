package com.example.data.network

import com.chenxyu.retrofit.adapter.FlowCallAdapterFactory
import com.example.data.BASE_URL_NEWS
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class NewsRetrofitCreator {

    fun getNewsRetrofit(): NewsRetrofitService {
        return Retrofit.Builder()
            .baseUrl(BASE_URL_NEWS)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(FlowCallAdapterFactory())
            .build()
            .create(NewsRetrofitService::class.java)
    }

}